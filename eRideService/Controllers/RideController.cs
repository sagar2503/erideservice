﻿using Google.Cloud.Firestore;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using eRideService.DataModel;
using eRideService.Entity;

namespace eRideService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RideController : ControllerBase
    {
        
        // GET api/values
        [HttpGet]
        public ActionResult<List<UserRide>> Get()
        {            
            UserRidesModel uRideModel = new UserRidesModel();
            List<UserRide> uRideEnt =  uRideModel.GetAllRides();
            return uRideEnt;
        }

        /// <summary>
        /// For ex: https://localhost:44313/api/ride/111
        /// </summary>
        /// <param name="rideid"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<UserRide> GetRideByRideid(string rideid)
        {
            UserRidesModel uRideModel = new UserRidesModel();
            UserRide uRideEnt = uRideModel.GetRideByRideId(rideid);
            return uRideEnt;
        }

        /// <summary>
        /// For ex: https://localhost:44313/api/ride/rideid/111
        /// For ex: https://localhost:44313/api/ride/ridestatus/A
        /// </summary>
        /// <param name="param"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{param}/{id}")]       
        public ActionResult<UserRide> GetRideByParam(string param ,string id)
        {
            UserRidesModel uRideModel = new UserRidesModel();
            UserRide uRideEnt = uRideModel.GetRideByParamId(param, id);
            return uRideEnt;
        }      


        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
