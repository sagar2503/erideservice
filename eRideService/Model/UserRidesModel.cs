﻿using eRideService.Entity;
using Google.Cloud.Firestore;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace eRideService.DataModel
{

    public class UserRidesModel
    {
        private const string userridecollection = "userrides";
        private const string projectId = "fir-654fc";

        public List<UserRide> GetAllRides()
        {
            Task<List<UserRide>> task = GetAllRidesAsEntityCollection();
            return task.Result;
        }       

        public UserRide GetAllUserRides(string userId)
        {
            Task<UserRide> task = GetRideAsEntity(userId);
            return task.Result;
        }

        public UserRide GetRideByRideId(string rideId)
        {
            Task<UserRide> task = GetRideAsEntity(rideId);
            return task.Result;
        }

        public UserRide GetRideByParamId(string param,string id)
        {
            Task<UserRide> task = GetRideAsEntityByParamId(param, id);
            return task.Result;
        }

        #region PRIVATE METHODS
        private async Task<List<UserRide>> GetAllRidesAsEntityCollection()
        {
            FirestoreDb db = GetFirestoreDBConnection();
            List<UserRide> rideCollection = new List<UserRide>();
            // You can create references directly from FirestoreDb:
            CollectionReference userrides = db.Collection(userridecollection);
            
            QuerySnapshot snapshot = await userrides.GetSnapshotAsync();
            if (snapshot != null && snapshot.Count > 0)
            {
                foreach (DocumentSnapshot doc in snapshot)
                {
                    rideCollection.Add(doc.ConvertTo<UserRide>());
                }
                return rideCollection;
            }
            else
            {
                return null;
            }
        }

        private async Task<UserRide> GetRideAsEntity(string rideid)
        {
            FirestoreDb db = GetFirestoreDBConnection();           
            // You can create references directly from FirestoreDb:
            CollectionReference userrides = db.Collection(userridecollection);
            Query query = userrides.WhereEqualTo("rideid", rideid);           
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();
           
            if (querySnapshot.Documents.Count > 0 && querySnapshot.Documents[0].Exists)
            {               
                UserRide uride = querySnapshot.Documents[0].ConvertTo<UserRide>();
                return uride;                
            }        
            else
                return null;
        }

        private async Task<UserRide> GetRideAsEntityByParamId(string param,string id)
        {
            FirestoreDb db = GetFirestoreDBConnection();
            // You can create references directly from FirestoreDb:
            CollectionReference userrides = db.Collection(userridecollection);
            Query query = userrides.WhereEqualTo(param, id);
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();

            if (querySnapshot.Documents.Count > 0 && querySnapshot.Documents[0].Exists)
            {
                UserRide uride = querySnapshot.Documents[0].ConvertTo<UserRide>();
                return uride;
            }
            else
                return null;
        }

        private async Task<UserRide> GetRideAsEntity(int rideId)
        {
            FirestoreDb db = GetFirestoreDBConnection();           
            CollectionReference userrides = db.Collection(userridecollection);           
            Query query = userrides.WhereEqualTo("rideid", "111");
            QuerySnapshot querySnapshot = await query.GetSnapshotAsync();
            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
            {
                return documentSnapshot.ConvertTo<UserRide>();
            }
            return null;
        }

        private static FirestoreDb GetFirestoreDBConnection()
        {            
            FirestoreDb db = FirestoreDb.Create(projectId);
            return db;
        }

        #endregion
    }
}
