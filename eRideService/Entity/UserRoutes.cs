﻿using Google.Cloud.Firestore;

namespace eRideService.Entity
{
    [FirestoreData]
    public class UserRoutes
    {
        [FirestoreProperty]
        public string routeid { get; set; }

        [FirestoreProperty]
        public string day { get; set; }

        [FirestoreProperty]
        public GeoPoint endpoint { get; set; }

        [FirestoreProperty]
        public GeoPoint startpoint { get; set; }

        [FirestoreProperty]
        public string userid { get; set; }
    }
}
