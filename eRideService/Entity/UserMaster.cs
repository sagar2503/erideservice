﻿using Google.Cloud.Firestore;

namespace eRideService.Entity
{
    [FirestoreData]
    public class UserMaster
    {
        [FirestoreProperty]
        public string userid { get; set; }

        [FirestoreProperty]
        public string firstname { get; set; }

        [FirestoreProperty]
        public string lastname { get; set; }

        [FirestoreProperty]
        public string password { get; set; }

        [FirestoreProperty]
        public string company { get; set; }

        [FirestoreProperty]
        public string userstatus { get; set; }
    }
}
