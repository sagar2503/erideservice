﻿using Google.Cloud.Firestore;

namespace eRideService.Entity
{
    [FirestoreData]
    public class UserAddresses
    {
        [FirestoreProperty]
        public string addressid { get; set; }

        [FirestoreProperty]
        public string userid { get; set; }

        [FirestoreProperty]
        public string address { get; set; }

        [FirestoreProperty]
        public bool addresstype { get; set; }

        [FirestoreProperty]
        public string defaultaddress { get; set; }

        [FirestoreProperty]
        public string email { get; set; }

        [FirestoreProperty]
        public GeoPoint geolocation { get; set; }

        [FirestoreProperty]
        public int mobilenr { get; set; }
    }
}
