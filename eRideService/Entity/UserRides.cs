﻿using Google.Cloud.Firestore;
using System.Threading.Tasks;
using System.Collections;

namespace eRideService.Entity
{
    [FirestoreData]
    public class UserRide
    {
        [FirestoreProperty]
        public string rideid { get; set; }

        [FirestoreProperty]
        public int ridecredit { get; set; }

        [FirestoreProperty]
        public double ridedistance { get; set; }

        [FirestoreProperty]
        public GeoPoint start { get; set; }

        [FirestoreProperty]
        public GeoPoint end { get; set; }

        [FirestoreProperty]
        public int totalpasangers { get; set; }

        [FirestoreProperty]
        public string ridestatus { get; set; }

        /// <summary>
        /// THIS FIELD DOES NOT EXIST IN FIRESTORE UserRides collection , still bindind works
        /// </summary>
        [FirestoreProperty]
        public string abc { get; set; }

    }
}
