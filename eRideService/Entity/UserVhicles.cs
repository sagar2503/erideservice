﻿using Google.Cloud.Firestore;

namespace eRideService.Entity
{
    [FirestoreData]
    public class UserVhicles
    {
        [FirestoreProperty]
        public string vehicleid { get; set; }

        [FirestoreProperty]
        public string userid { get; set; }

        [FirestoreProperty]
        public string vehiclenr { get; set; }

        [FirestoreProperty]
        public int seatingcapacity { get; set; }

        [FirestoreProperty]
        public string model { get; set; }

        [FirestoreProperty]
        public string make { get; set; }

        [FirestoreProperty]
        public string color { get; set; }
    }
}
